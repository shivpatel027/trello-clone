const express = require('express');
const app = express();
const port = 3000; // default

app.get('/', (req, res) => {
    res.send("Hello to the trello clone backend!");
})

app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
})